import cv2
import os
import csv
import re
from sfta import SegmentationFractalTextureAnalysis

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename))
        if img is not None:
            images.append(img)
    return images

def add_column_in_csv(input_file, output_file, transform_row):
    with open(input_file, 'r') as read_obj, \
            open(output_file, 'w', newline='') as write_obj:
        csv_reader = csv.reader(read_obj)
        csv_writer = csv.writer(write_obj)
        for row in csv_reader:
            transform_row(row, csv_reader.line_num)
            csv_writer.writerow(row)

images = []
images = load_images_from_folder("dataset")
nt = 5
banyak = nt*2
sfta = SegmentationFractalTextureAnalysis(nt)
loccsv = 'csv2/data5.csv'
loc = 'data5.csv'
with open(loccsv, 'w', newline='') as f:
    thewriter = csv.writer(f)
    head = []
    for i in range(1,banyak+1):
        col = 'D'+str(i)+','+'V'+str(i)+','+'A'+str(i)
        head.append(col)
    thewriter.writerow(head)
    for i in range(0, len(images)):
        sftavalue = sfta.feature_vector(images[i])
        thewriter.writerow(sftavalue)

f = open('class2.txt', 'r')
f = f.read()
file_class= re.split(',|\n',f)
add_column_in_csv(loccsv, loc, lambda row, line_num: row.append(file_class[line_num-1]))

print('berhasil')