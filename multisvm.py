import pandas as pd
from sklearn import svm, metrics
from sklearn.model_selection import train_test_split
from sfta import SegmentationFractalTextureAnalysis
import cv2

imgtest = cv2.imread('dataset/mdb028.pgm')
sfta = SegmentationFractalTextureAnalysis(5)
sftavalue = sfta.feature_vector(imgtest)

cancerdata = pd.read_csv("multicsv/data5.csv")
data = cancerdata.drop('Class', axis=1)
target = cancerdata['Class']

X_train, y_train, X_test  = data, target, sftavalue
svclassifier = svm.SVC(decision_function_shape='ovr', gamma='auto')
svclassifier.fit(X_train, y_train)#create model

y_pred = svclassifier.predict(X_test.reshape(1,-1))#predict

if (y_pred == 0):
    print("Normal")
elif(y_pred == 1):
    print("Jinak")
elif(y_pred == 2):
    print("Ganas")

cv2.imshow("Test Image",imgtest)
cv2.waitKey()

