import cv2

file_img = cv2.imread('test/218.png')
file_roi = cv2.imread('test/218-1.png')
file_save = 'dataset2/mdb400.png'

file_roi = cv2.cvtColor(file_roi, cv2.COLOR_RGB2GRAY)

masking = cv2.bitwise_and(file_img, file_img, mask=file_roi)
cv2.imwrite(file_save, masking)