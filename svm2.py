import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import svm, metrics
from time import process_time
import seaborn as sns; sns.set(font_scale=1.2)

t1_start = process_time()
cancerdata = pd.read_csv("csv2/data3.csv")
data = cancerdata.drop('Class', axis=1)
target = cancerdata['Class']

X_train, X_test, y_train, y_test = train_test_split(data, target,test_size=0.3, random_state=100)
svclassifier = svm.SVC(kernel='linear')
svclassifier.fit(X_train, y_train)#create model
y_pred = svclassifier.predict(X_test)
y_train_model = svclassifier.predict(X_train)

print('weights: ')
print(svclassifier.coef_)
print('Intercept: ')
print(svclassifier.intercept_)

print(metrics.confusion_matrix(y_test, y_pred))
print("Accuracy:",round(metrics.accuracy_score(y_test, y_pred),4))
print("Precision:",round(metrics.precision_score(y_test, y_pred),4))
print("Recall:",round(metrics.recall_score(y_test, y_pred),4))
print("F1-Score:",round(metrics.f1_score(y_test, y_pred),4))

#ini menguji data latih
print(metrics.confusion_matrix(y_train, y_train_model))
print("Accuracy:",round(metrics.accuracy_score(y_train, y_train_model),4))
print("Precision:",round(metrics.precision_score(y_train, y_train_model),4))
print("Recall:",round(metrics.recall_score(y_train, y_train_model),4))
print("F1-Score:",round(metrics.f1_score(y_train, y_train_model),4))

mse = metrics.mean_absolute_error(y_train, svclassifier.predict(X_train))
print("Training Set Mean Absolute Error: %.4f" % mse)

mse = metrics.mean_absolute_error(y_test, svclassifier.predict(X_test))
print("Test Set Mean Absolute Error: %.4f" % mse)

t1_stop = process_time()
print("Lama: ",t1_stop-t1_start)

# print('Training accuracy (%) : ',np.mean(y_train == y_train)*100)
# print('Test accuracy (%) : ',np.mean(y_pred == y_test)*100)

# # Get the separating hyperplane
# w = svclassifier.coef_[0]
# a = -w[0] / w[1]
# xx = np.linspace(30, 60)
# yy = a * xx - (svclassifier.intercept_[0]) / w[1]
#
# # Plot the parallels to the separating hyperplane that pass through the support vectors
# b = svclassifier.support_vectors_[0]
# yy_down = a * xx + (b[1] - a * b[0])
# b = svclassifier.support_vectors_[-1]
# yy_up = a * xx + (b[1] - a * b[0])
#
# sns.lmplot('D1', 'V2', data=cancerdata, hue='Class',
#            palette='Set1', fit_reg=False, scatter_kws={"s": 70})
# plt.plot(xx, yy, linewidth=2, color='black');
# plt.show()
# plt.plot(xx, yy, linewidth=2, color='black');

# from sklearn.preprocessing import StandardScaler
# sc = StandardScaler()
# X_train = sc.fit_transform(X_train)
# X_test = sc.transform(X_test)
#
# X_set, y_set = X_train, y_train
# X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01),
#                      np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))
# Xpred = np.array([X1.ravel(), X2.ravel()] + [np.repeat(0, X1.ravel().size) for _ in range(11)]).T
# pred = cv2.trasvclassifier.predict(Xpred).reshape(X1.shape)
# plt.contourf(X1, X2, pred,
#              alpha = 0.75, cmap = ListedColormap(('red', 'green')))
# plt.xlim(X1.min(), X1.max())
# plt.ylim(X2.min(), X2.max())
# for i, j in enumerate(np.unique(y_set)):
#     plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
#                 c = ListedColormap(('red', 'green'))(i), label = j)
# plt.title('SVM (Training set)')
# plt.xlabel('Usia')
# plt.ylabel('Estimasi Gaji')
# plt.legend()
# plt.show()
