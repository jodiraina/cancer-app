import csv

with open('csv2/data3.csv', newline='') as f:
    reader = csv.reader(f)
    data = list(reader)

data.pop(0)
normal=[]
abnormal=[]

for i in range(len(data)):
    if(data[i][18]=='0'):
        normal.append((data[i]))
    else:
        abnormal.append(data[i])

for i in range(len(normal)):
    for x in range(0,18):
        normal[i][x]=float(normal[i][x])

for i in range(len(abnormal)):
    for x in range(0,18):
        abnormal[i][x]=float(abnormal[i][x])

D1=0;D2=0;D3=0;D4=0;D5=0;D6=0
V1=0;V2=0;V3=0;V4=0;V5=0;V6=0
A1=0;A2=0;A3=0;A4=0;A5=0;A6=0

fractal_normal=[]
fractal_abnormal=[]
mean_normal=[]
mean_abnormal=[]
area_normal=[]
area_abnormal=[]
for i in range(len(normal)):
    D = round((normal[i][0]+normal[i][3]+normal[i][6]+normal[i][9]+normal[i][12]+normal[i][15])/6, 3)
    fractal_normal.append(D)
print("Range Fractal Normal = 0 atau "+str(min(fractal_normal))+" - "+str(max(fractal_normal)))
for i in range(len(abnormal)):
    D = round((abnormal[i][0]+abnormal[i][3]+abnormal[i][6]+abnormal[i][9]+abnormal[i][12]+abnormal[i][15])/6, 3)
    fractal_abnormal.append(D)
print("Range Fractal Abnormal = "+str(min(fractal_abnormal))+" - "+str(max(fractal_abnormal)))

for i in range(len(normal)):
    V = round((normal[i][1]+normal[i][4]+normal[i][7]+normal[i][10]+normal[i][13]+normal[i][16])/6, 3)
    mean_normal.append(V)
print("Mean Normal = 0 atau "+str(min(mean_normal))+" - "+str(max(mean_normal)))

for i in range(len(abnormal)):
    V = round((abnormal[i][1]+abnormal[i][4]+abnormal[i][7]+abnormal[i][10]+abnormal[i][13]+abnormal[i][16])/6, 3)
    mean_abnormal.append(V)
print("Mean Abnormal = "+str(min(mean_abnormal))+" - "+str(max(mean_abnormal)))

for i in range(len(normal)):
    A = round((normal[i][2]+normal[i][5]+normal[i][9]+normal[i][11]+normal[i][14]+normal[i][17])/6, 3)
    area_normal.append(A)
print("Area Normal = 0 atau "+str(min(area_normal))+" - "+str(max(area_normal)))

for i in range(len(abnormal)):
    A = round((abnormal[i][2]+abnormal[i][5]+abnormal[i][9]+abnormal[i][11]+abnormal[i][14]+abnormal[i][17])/6, 3)
    area_abnormal.append(A)
print("Area Abnormal = 0 atau "+str(min(area_abnormal))+" - "+str(max(area_abnormal)))

# print(sum(fractal_normal)/215)
# print(sum(fractal_abnormal)/185)
# print(sum(mean_normal)/215)
# print(sum(mean_abnormal)/185)
# print(sum(area_normal)/215)
# print(sum(area_abnormal)/185)

for i in range(len(normal)):
    D1 = normal[i][0] + D1
    D2 = normal[i][3] + D2
    D3 = normal[i][6] + D3
    D4 = normal[i][9] + D4
    D5 = normal[i][12] + D5
    D6 = normal[i][15] + D6
D1 = round(D1/215,3)
D2 = round(D2/215,3)
D3 = round(D3/215,3)
D4 = round(D4/215,3)
D5 = round(D5/215,3)
D6 = round(D6/215,3)
fractal_normal = [D1,D2,D3,D4,D5,D6]
print("Range Fractal Normal = 0 atau "+str(min(fractal_normal))+" - "+str(max(fractal_normal)))

for i in range(len(abnormal)):
    D1 = abnormal[i][0] + D1
    D2 = abnormal[i][3] + D2
    D3 = abnormal[i][6] + D3
    D4 = abnormal[i][9] + D4
    D5 = abnormal[i][12] + D5
    D6 = abnormal[i][15] + D6
D1 = round(D1/185,3)
D2 = round(D2/185,3)
D3 = round(D3/185,3)
D4 = round(D4/185,3)
D5 = round(D5/185,3)
D6 = round(D6/185,3)
fractal_abnormal = [D1,D2,D3,D4,D5,D6]
print("Range Fractal Abnormal = "+str(min(fractal_abnormal))+" - "+str(max(fractal_abnormal)))

for i in range(len(normal)):
    V1 = normal[i][1] + V1
    V2 = normal[i][4] + V2
    V3 = normal[i][7] + V3
    V4 = normal[i][10] + V4
    V5 = normal[i][13] + V5
    V6 = normal[i][16] + V6
V1 = round(V1/215,3)
V2 = round(V2/215,3)
V3 = round(V3/215,3)
V4 = round(V4/215,3)
V5 = round(V5/215,3)
V6 = round(V6/215,3)
mean_normal = [V1,V2,V3,V4,V5,V6]
print("Mean Normal = 0 atau "+str(min(mean_normal))+" - "+str(max(mean_normal)))

for i in range(len(abnormal)):
    V1 = abnormal[i][1] + V1
    V2 = abnormal[i][4] + V2
    V3 = abnormal[i][7] + V3
    V4 = abnormal[i][10] + V4
    V5 = abnormal[i][13] + V5
    V6 = abnormal[i][16] + V6
V1 = round(V1/185,3)
V2 = round(V2/185,3)
V3 = round(V3/185,3)
V4 = round(V4/185,3)
V5 = round(V5/185,3)
V6 = round(V6/185,3)
mean_abnormal = [V1,V2,V3,V4,V5,V6]
print("Mean Abnormal = "+str(min(mean_abnormal))+" - "+str(max(mean_abnormal)))

for i in range(len(normal)):
    D1 = normal[i][2] + D1
    D2 = normal[i][5] + D2
    D3 = normal[i][8] + D3
    D4 = normal[i][11] + D4
    D5 = normal[i][14] + D5
    D6 = normal[i][17] + D6
D1 = round(D1/215,3)
D2 = round(D2/215,3)
D3 = round(D3/215,3)
D4 = round(D4/215,3)
D5 = round(D5/215,3)
D6 = round(D6/215,3)
area_normal = [D1,D2,D3,D4,D5,D6]
print("Area Normal = 0 atau "+str(min(area_normal))+" - "+str(max(area_normal)))

for i in range(len(abnormal)):
    D1 = abnormal[i][2] + D1
    D2 = abnormal[i][5] + D2
    D3 = abnormal[i][8] + D3
    D4 = abnormal[i][11] + D4
    D5 = abnormal[i][14] + D5
    D6 = abnormal[i][17] + D6
D1 = round(D1/185,3)
D2 = round(D2/185,3)
D3 = round(D3/185,3)
D4 = round(D4/185,3)
D5 = round(D5/185,3)
D6 = round(D6/185,3)
area_abnormal = [D1,D2,D3,D4,D5,D6]
print("Area abnormal = "+str(min(area_abnormal))+" - "+str(max(area_abnormal)))