from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui
from PyQt5.QtGui import QPixmap
from sklearn import svm
from sfta import SegmentationFractalTextureAnalysis
import numpy as np
import pandas as pd
import sys
import cv2
from time import process_time
import matplotlib.pyplot as plt

class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.imagepath = ''
        self.pathicon = 'assets/icons/'
        self.datasfta = ''
        self.setWindowIcon(QtGui.QIcon('assets/images/Logo.png'))
        self.setWindowTitle('Aplikasi Klasifikasi Kanker Payudara')
        self.setStyleSheet('background-color: #f4ecec')

        # set the size of window
        self.Width = 1600
        self.height = int(0.618 * self.Width)
        self.resize(self.Width, self.height)

        self.LabelJudul = QLabel('Aplikasi Klasifikasi Kanker Payudara\n'
                                       'Penerapan Texture Based Extraction Untuk Mendeteksi Kanker Pada Massa Citra Mammogram')
        self.LabelJudul.setFont(QtGui.QFont("SansSerif", 14, QtGui.QFont.Black))
        self.LabelJudul.setAlignment(QtCore.Qt.AlignCenter)

        self.Logo = QLabel(self)
        pixmaplogo = QPixmap('assets/images/Logo.png')
        pixmaplogo = pixmaplogo.scaledToWidth(220)
        pixmaplogo = pixmaplogo.scaledToHeight(220)
        self.Logo.setPixmap(pixmaplogo)
        self.Logo.resize(pixmaplogo.width(), pixmaplogo.height())
        self.Logo.setAlignment(QtCore.Qt.AlignCenter)

        self.color_btnSidebar = '#92bcc8'
        # add all widgets
        self.btn_1 = QPushButton('Upload File', self)
        self.btn_1.setIcon(QtGui.QIcon(self.pathicon+'inbox-upload.png'))
        self.btn_1.setStyleSheet('background-color: white')
        self.btn_2 = QPushButton('Segmentasi', self)
        self.btn_2.setIcon(QtGui.QIcon(self.pathicon + 'layers.png'))
        self.btn_2.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_3 = QPushButton('Morfologi', self)
        self.btn_3.setIcon(QtGui.QIcon(self.pathicon + 'arrow-circle-225.png'))
        self.btn_3.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_4 = QPushButton('Ekstraksi Fitur', self)
        self.btn_4.setIcon(QtGui.QIcon(self.pathicon + 'table.png'))
        self.btn_4.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_5 = QPushButton('Hasil Klasifikasi', self)
        self.btn_5.setIcon(QtGui.QIcon(self.pathicon + 'report.png'))
        self.btn_5.setStyleSheet('background-color:'+self.color_btnSidebar)
        self.sbimg = QLabel(self)
        pixmapsbimg = QPixmap('assets/images/itenas.png')
        self.sbimg.setPixmap(pixmapsbimg)
        self.sbimg.setAlignment(QtCore.Qt.AlignCenter)

        self.btn_1.setFont(QtGui.QFont("Calibri", 12))
        self.btn_2.setFont(QtGui.QFont("Calibri", 12))
        self.btn_3.setFont(QtGui.QFont("Calibri", 12))
        self.btn_4.setFont(QtGui.QFont("Calibri", 12))
        self.btn_5.setFont(QtGui.QFont("Calibri", 12))

        self.btn_1.clicked.connect(self.button1)
        self.btn_2.clicked.connect(self.button2)
        self.btn_3.clicked.connect(self.button3)
        self.btn_4.clicked.connect(self.button4)
        self.btn_5.clicked.connect(self.button5)

        # add tabs
        self.tab1 = self.ui1()
        self.tab2 = self.ui2()
        self.tab3 = self.ui3()
        self.tab4 = self.ui4()
        self.tab5 = self.ui5()

        self.initUI()

    def initUI(self):
        navbar_layout = QHBoxLayout()
        navbar_layout.addWidget(self.Logo)
        navbar_layout.addWidget(self.LabelJudul)
        navbar_layout.setContentsMargins(30,0,0,0)
        navbar_layout.setStretch(0, 40)
        navbar_layout.setStretch(1,200)
        navbar_widget = QWidget()
        navbar_widget.setStyleSheet("background-color: #c5ec86;")
        navbar_widget.setLayout(navbar_layout)

        left_layout_img = QVBoxLayout()
        left_layout_img.addWidget(self.sbimg)
        left_layout_img_widget = QWidget()
        left_layout_img_widget.setLayout(left_layout_img)

        left_layout = QVBoxLayout()
        left_layout.addWidget(self.btn_1)
        left_layout.addWidget(self.btn_2)
        left_layout.addWidget(self.btn_3)
        left_layout.addWidget(self.btn_4)
        left_layout.addWidget(self.btn_5)
        left_layout.addStretch(5)
        left_layout.setSpacing(20)
        left_widget = QWidget()
        left_widget.setLayout(left_layout)

        left_layout = QVBoxLayout()
        left_layout.addWidget(left_widget)
        left_layout.addWidget(left_layout_img_widget)
        left_widget = QWidget()
        left_widget.setStyleSheet("background-color: #c5ec86;")
        left_widget.setLayout(left_layout)

        self.right_widget = QTabWidget()
        self.right_widget.tabBar().setObjectName("mainTab")

        self.right_widget.addTab(self.tab1, '')
        self.right_widget.addTab(self.tab2, '')
        self.right_widget.addTab(self.tab3, '')
        self.right_widget.addTab(self.tab4, '')
        self.right_widget.addTab(self.tab5, '')

        self.right_widget.setCurrentIndex(0)
        self.right_widget.setStyleSheet('''QTabBar::tab{width: 0; \
            height: 0; margin: 0; padding: 0; border: none;}''')

        main_layout = QHBoxLayout()
        main_layout.addWidget(left_widget)
        main_layout.addWidget(self.right_widget)
        main_layout.setStretch(0, 40)
        main_layout.setStretch(1, 200)
        main_widget = QWidget()
        main_widget.setLayout(main_layout)

        main_layout = QVBoxLayout()
        main_layout.addWidget(navbar_widget)
        main_layout.addWidget(main_widget)
        main_widget = QWidget()
        main_widget.setLayout(main_layout)

        self.setCentralWidget(main_widget)

    # -----------------
    # buttons

    def button1(self):
        self.right_widget.setCurrentIndex(0)
        self.btn_1.setStyleSheet('background-color: white')
        self.btn_2.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_3.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_4.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_5.setStyleSheet('background-color: '+self.color_btnSidebar)

    def button2(self):
        self.right_widget.setCurrentIndex(1)
        self.btn_1.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_2.setStyleSheet('background-color: white')
        self.btn_3.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_4.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_5.setStyleSheet('background-color: '+self.color_btnSidebar)

    def button3(self):
        self.right_widget.setCurrentIndex(2)
        self.btn_1.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_2.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_3.setStyleSheet('background-color: white')
        self.btn_4.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_5.setStyleSheet('background-color: '+self.color_btnSidebar)

    def button4(self):
        self.right_widget.setCurrentIndex(3)
        self.btn_1.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_2.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_3.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_4.setStyleSheet('background-color: white')
        self.btn_5.setStyleSheet('background-color: '+self.color_btnSidebar)

        if self.datasfta != '':
            self.inserttables()

    def button5(self):
        self.right_widget.setCurrentIndex(4)
        self.btn_1.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_2.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_3.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_4.setStyleSheet('background-color: '+self.color_btnSidebar)
        self.btn_5.setStyleSheet('background-color: white')

    # -----------------
    # pages

    def ui1(self):
        main_layout = QVBoxLayout()
        color_btnContent = '#92bcc8'

        btnOpen = QPushButton('Open Image', self)
        btnOpen.setIcon(QtGui.QIcon('assets/icons/folder-open-image.png'))
        btnOpen.setStyleSheet('background-color: '+color_btnContent)
        btnOpen.clicked.connect(self.clickmethodopenimg)

        self.edtPath = QLineEdit(self)
        self.edtPath.setReadOnly(True)

        self.GambarInput = QLabel(self)
        self.GambarInput.setAlignment(QtCore.Qt.AlignCenter)

        btnStart = QPushButton('Mulai Proses Klasifikasi', self)
        btnStart.setIcon(QtGui.QIcon('assets/icons/clock-select-remain.png'))
        btnStart.setStyleSheet('background-color: ' +color_btnContent)
        btnStart.clicked.connect(self.clickmethodstartimg)

        main_layout.addWidget(btnOpen)
        main_layout.addWidget(self.edtPath)
        main_layout.addWidget(self.GambarInput)
        main_layout.addWidget(btnStart)

        # main_layout.addStretch()
        main = QWidget()
        main.setLayout(main_layout)

        return main

    def ui2(self):
        main_layout = QGridLayout()
        self.GambarSegment_kmeans = QLabel(self)
        self.GambarSegment_kmeans.setAlignment(QtCore.Qt.AlignCenter)
        self.GambarSegment_thresh = QLabel(self)
        self.GambarSegment_thresh.setAlignment(QtCore.Qt.AlignCenter)
        self.LabelKmeans = QLabel('Hasil K-Means Clustering')
        self.LabelKmeans.setAlignment(QtCore.Qt.AlignCenter)
        self.LabelKmeans.setFont(QtGui.QFont("Calibri", 12))
        self.LabelThresh = QLabel('Hasil Thresholding')
        self.LabelThresh.setAlignment(QtCore.Qt.AlignCenter)
        self.LabelThresh.setFont(QtGui.QFont("Calibri", 12))

        main_layout.addWidget(self.GambarSegment_kmeans, 0, 0)
        main_layout.addWidget(self.GambarSegment_thresh, 0, 1)
        main_layout.addWidget(self.LabelKmeans, 1, 0)
        main_layout.addWidget(self.LabelThresh, 1, 1)

        main = QWidget()
        main.setLayout(main_layout)

        return main

    def ui3(self):
        main_layout = QGridLayout()
        self.GambarOpening = QLabel(self)
        self.GambarOpening.setAlignment(QtCore.Qt.AlignCenter)
        self.GambarMasking = QLabel(self)
        self.GambarMasking.setAlignment(QtCore.Qt.AlignCenter)
        self.LabelOpening = QLabel('Hasil Opening')
        self.LabelOpening.setAlignment(QtCore.Qt.AlignCenter)
        self.LabelOpening.setFont(QtGui.QFont("Calibri", 12))
        self.LabelMasking = QLabel('Hasil Masking')
        self.LabelMasking.setAlignment(QtCore.Qt.AlignCenter)
        self.LabelMasking.setFont(QtGui.QFont("Calibri", 12))

        main_layout.addWidget(self.GambarOpening,0,0)
        main_layout.addWidget(self.GambarMasking,0,1)
        main_layout.addWidget(self.LabelOpening,1,0)
        main_layout.addWidget(self.LabelMasking,1,1)

        main = QWidget()
        main.setLayout(main_layout)
        return main

    def ui4(self):
        main_layout = QVBoxLayout()
        self.creatingTables()
        main_layout.addWidget(self.tableWidget)
        main = QWidget()
        main.setLayout(main_layout)
        return main

    def ui5(self):
        main_layout = QGridLayout()
        self.Hasil = QLabel(self)
        self.Hasil.setAlignment(QtCore.Qt.AlignCenter)
        self.edtKlasifikasi = QLabel('Hasil Klasifikasi: ')
        self.edtKlasifikasi.setAlignment(QtCore.Qt.AlignCenter)
        self.edtKlasifikasi.setFont(QtGui.QFont("Calibri", 12))
        main_layout.addWidget(self.Hasil)
        main_layout.addWidget(self.edtKlasifikasi)
        main = QWidget()
        main.setLayout(main_layout)

        return main

    def clickmethodopenimg(self):
        fname = QFileDialog.getOpenFileName(self, "Open Image", "",
                                                  "Image files (*.png *.pgm)")
        if (fname!=('', '')):
            imagePath = fname[0]
            self.pixmap = QPixmap(imagePath)
            self.pixmap = self.pixmap.scaledToWidth(512)
            self.pixmap = self.pixmap.scaledToHeight(512)
            self.GambarInput.setPixmap(self.pixmap)
            self.GambarInput.resize(self.pixmap.width(), self.pixmap.height())
            self.edtPath.setText(imagePath)

            imgpath = self.edtPath.text()
            self.image = cv2.imread(imgpath)
            width = 1024
            height = 1024
            dim = (width, height)
            self.image = cv2.resize(self.image, dim, interpolation=cv2.INTER_AREA)
            # cv2.imshow('fssd',self.image)
    def clickmethodstartimg(self):
        if (self.edtPath.text()!=''):
            self.kmeans()
            self.thresholding()
            self.opening()
            self.masking()
            self.sftamethod()
            self.klasifikasi()
            QMessageBox.about(self, "Klasfikasi", "Proses Selesai")
        else:
            QMessageBox.about(self, "Error", "Silahkan Pilih Gambar dahulu")

    def kmeans(self):
        # print(self.image.shape)
        Z = self.image.reshape((-1, 3))

        Z = np.float32(Z)  # convert to np.float32

        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.2)
        K = 4
        ret, label, center = cv2.kmeans(Z, K, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
        center = np.uint8(center)
        res = center[label.flatten()]
        self.k_means = res.reshape((self.image.shape))

        cv2.imwrite('hasil/k-means.png', self.k_means)
        pixmap = QPixmap('hasil/k-means.png')
        pixmap = pixmap.scaledToWidth(512)
        pixmap = pixmap.scaledToHeight(512)
        self.GambarSegment_kmeans.setPixmap(pixmap)
        self.GambarSegment_kmeans.resize(pixmap.width(), pixmap.height())

    def thresholding(self):
        gray = cv2.cvtColor(self.k_means, cv2.COLOR_BGR2GRAY)
        ret, self.thresh = cv2.threshold(gray, 158, 255, cv2.THRESH_BINARY)

        # cv2.imshow('',self.thresh)
        self.imgrect()
        cv2.imwrite('hasil/thresh.png', self.triangle2)
        pixmap = QPixmap('hasil/thresh.png')
        pixmap = pixmap.scaledToWidth(512)
        pixmap = pixmap.scaledToHeight(512)
        self.GambarSegment_thresh.setPixmap(pixmap)
        self.GambarSegment_thresh.resize(pixmap.width(), pixmap.height())

    def opening(self):
        kernel = np.ones((15, 15), np.uint8)
        self.imgopening = cv2.morphologyEx(self.triangle2, cv2.MORPH_OPEN, kernel)
        cv2.imwrite('hasil/opening.png', self.imgopening)
        pixmap = QPixmap('hasil/opening.png')
        pixmap = pixmap.scaledToWidth(512)
        pixmap = pixmap.scaledToHeight(512)
        self.GambarOpening.setPixmap(pixmap)
        self.GambarOpening.resize(pixmap.width(), pixmap.height())

    def masking(self):
        self.imgext = cv2.bitwise_and(self.image, self.image, mask=self.imgopening)
        # self.imgext = cv2.cvtColor(self.imgext, cv2.COLOR_BGR2GRAY)
        cv2.imwrite('hasil/masking.png', self.imgext)
        pixmap = QPixmap('hasil/masking.png')
        pixmap = pixmap.scaledToWidth(512)
        pixmap = pixmap.scaledToHeight(512)
        self.GambarMasking.setPixmap(pixmap)
        self.GambarMasking.resize(pixmap.width(), pixmap.height())

    def imgrect(self):
        pt1 = (1024, 0)
        pt2 = (450, 0)
        pt3 = (1024, 1024)

        pt4 = (0, 0)
        pt5 = (600, 0)
        pt6 = (0, 1024)

        #
        # pt1 = (512, 0)
        # pt2 = (225, 0)
        # pt3 = (512, 512)
        #
        # pt4 = (0, 0)
        # pt5 = (300, 0)
        # pt6 = (0, 512)

        # pt1 = (256, 0)
        # pt2 = (110, 0)
        # pt3 = (256, 256)
        #
        # pt4 = (0, 0)
        # pt5 = (160, 0)
        # pt6 = (0, 256)

        # pt1 = (2048, 0)
        # pt2 = (900, 0)
        # pt3 = (2048, 2048)
        #
        # pt4 = (0, 0)
        # pt5 = (1100, 0)
        # pt6 = (0, 2048)

        cv2.circle(self.thresh, pt1, 0, (0, 0, 255), -1)
        cv2.circle(self.thresh, pt2, 0, (0, 0, 255), -1)
        cv2.circle(self.thresh, pt3, 0, (0, 0, 255), -1)

        cv2.circle(self.thresh, pt4, 0, (0, 0, 255), -1)
        cv2.circle(self.thresh, pt5, 0, (0, 0, 255), -1)
        cv2.circle(self.thresh, pt6, 0, (0, 0, 255), -1)

        triangle_cnt1 = np.array([pt1, pt2, pt3])
        triangle_cnt2 = np.array([pt4, pt5, pt6])
        self.triangle1 = cv2.drawContours(self.thresh, [triangle_cnt1], 0, 0, -1)
        self.triangle2 = cv2.drawContours(self.triangle1, [triangle_cnt2], 0, 0, -1)

    def sftamethod(self):
        sfta = SegmentationFractalTextureAnalysis(3)
        self.sftavalue = sfta.feature_vector(self.imgext)
        self.datasfta = self.dataframe()

    def klasifikasi(self):
        cancerdata = pd.read_csv("csv2/data3.csv")
        data = cancerdata.drop('Class', axis=1)
        target = cancerdata['Class']

        X_train, y_train, X_test = data, target, self.sftavalue
        svclassifier = svm.SVC(kernel='linear')
        svclassifier.fit(X_train, y_train)  # create model

        y_pred = svclassifier.predict(X_test.reshape(1, -1))

        if (y_pred == 0):
            self.kelas = "NORMAL"
        elif (y_pred == 1):
            self.kelas = "ABNORMAL"

        print('weights: ')
        weight = svclassifier.coef_.flatten()
        weight = weight.tolist()
        print(weight)
        print('Intercept: ')
        print(svclassifier.intercept_)
        sf=self.sftavalue.tolist()
        for w in range(len(weight)):
            for s in range(len(sf)):
                if w==s:
                    # print(weight[w], sf[s])
                    count =weight[w] * sf[s]
        hasil = count - svclassifier.intercept_
        print(hasil)
        if(hasil < 0 or sum(sf)==0):
            print("NORMAL")
        elif(hasil >=0):
            print("ABNORMAL")
        self.Hasil.setPixmap(self.pixmap)
        self.Hasil.resize(self.pixmap.width(), self.pixmap.height())
        self.edtKlasifikasi.setText('Hasil Klasifikasi: '+self.kelas)

        # # get the separating hyperplane
        # w = svclassifier.coef_[0]
        # a = -w[0] / w[1]
        # xx = np.linspace(-5, 5)
        # yy = a * xx - (svclassifier.intercept_[0]) / w[1]
        #
        # # plot the parallels to the separating hyperplane that pass through the
        # # support vectors
        # b = svclassifier.support_vectors_[0]
        # yy_down = a * xx + (b[1] - a * b[0])
        # b = svclassifier.support_vectors_[-1]
        # yy_up = a * xx + (b[1] - a * b[0])
        #
        # # plot the line, the points, and the nearest vectors to the plane
        # plt.plot(xx, yy, 'k-')
        # plt.plot(xx, yy_down, 'k--')
        # plt.plot(xx, yy_up, 'k--')
        #
        # plt.scatter(svclassifier.support_vectors_[:, 0], svclassifier.support_vectors_[:, 1],
        #             s=80, facecolors='none')
        # plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=plt.cm.Paired)
        #
        # plt.axis('tight')
        # plt.show()

    def dataframe(self):
        listoflists = []
        a_list = []
        for i in self.sftavalue:
            a_list.append(i)
            if (len(a_list) % 3 == 0):
                listoflists.append(list(a_list))
                a_list.remove(a_list[0])
                a_list.remove(a_list[0])
                a_list.remove(a_list[0])
        return listoflists

    def creatingTables(self):
        self.tableWidget = QTableWidget(self)
        self.tableWidget.setRowCount(20)
        self.tableWidget.setColumnCount(3)

    def inserttables(self):
        self.tableWidget.setItem(0, 0, QTableWidgetItem("Fractal Dimension"))
        self.tableWidget.setItem(0, 1, QTableWidgetItem("Mean"))
        self.tableWidget.setItem(0, 2, QTableWidgetItem("Area"))

        self.tableWidget.setItem(1, 0, QTableWidgetItem(str(self.datasfta[0][0])))
        self.tableWidget.setItem(1, 1, QTableWidgetItem(str(self.datasfta[0][1])))
        self.tableWidget.setItem(1, 2, QTableWidgetItem(str(self.datasfta[0][2])))

        self.tableWidget.setItem(2, 0, QTableWidgetItem(str(self.datasfta[1][0])))
        self.tableWidget.setItem(2, 1, QTableWidgetItem(str(self.datasfta[1][1])))
        self.tableWidget.setItem(2, 2, QTableWidgetItem(str(self.datasfta[1][2])))

        self.tableWidget.setItem(3, 0, QTableWidgetItem(str(self.datasfta[2][0])))
        self.tableWidget.setItem(3, 1, QTableWidgetItem(str(self.datasfta[2][1])))
        self.tableWidget.setItem(3, 2, QTableWidgetItem(str(self.datasfta[2][2])))

        self.tableWidget.setItem(4, 0, QTableWidgetItem(str(self.datasfta[3][0])))
        self.tableWidget.setItem(4, 1, QTableWidgetItem(str(self.datasfta[3][1])))
        self.tableWidget.setItem(4, 2, QTableWidgetItem(str(self.datasfta[3][2])))

        self.tableWidget.setItem(5, 0, QTableWidgetItem(str(self.datasfta[4][0])))
        self.tableWidget.setItem(5, 1, QTableWidgetItem(str(self.datasfta[4][1])))
        self.tableWidget.setItem(5, 2, QTableWidgetItem(str(self.datasfta[4][2])))

        self.tableWidget.setItem(6, 0, QTableWidgetItem(str(self.datasfta[5][0])))
        self.tableWidget.setItem(6, 1, QTableWidgetItem(str(self.datasfta[5][1])))
        self.tableWidget.setItem(6, 2, QTableWidgetItem(str(self.datasfta[5][2])))

        # self.tableWidget.setItem(7, 0, QTableWidgetItem(str(self.datasfta[6][0])))
        # self.tableWidget.setItem(7, 1, QTableWidgetItem(str(self.datasfta[6][1])))
        # self.tableWidget.setItem(7, 2, QTableWidgetItem(str(self.datasfta[6][2])))

        # self.tableWidget.setItem(8, 0, QTableWidgetItem(str(self.datasfta[7][0])))
        # self.tableWidget.setItem(8, 1, QTableWidgetItem(str(self.datasfta[7][1])))
        # self.tableWidget.setItem(8, 2, QTableWidgetItem(str(self.datasfta[7][2])))
        #
        # self.tableWidget.setItem(9, 0, QTableWidgetItem(str(self.datasfta[8][0])))
        # self.tableWidget.setItem(9, 1, QTableWidgetItem(str(self.datasfta[8][1])))
        # self.tableWidget.setItem(9, 2, QTableWidgetItem(str(self.datasfta[8][2])))
        #
        # self.tableWidget.setItem(10, 0, QTableWidgetItem(str(self.datasfta[9][0])))
        # self.tableWidget.setItem(10, 1, QTableWidgetItem(str(self.datasfta[9][1])))
        # self.tableWidget.setItem(10, 2, QTableWidgetItem(str(self.datasfta[9][2])))
        #
        # self.tableWidget.setItem(11, 0, QTableWidgetItem(str(self.datasfta[10][0])))
        # self.tableWidget.setItem(11, 1, QTableWidgetItem(str(self.datasfta[10][1])))
        # self.tableWidget.setItem(11, 2, QTableWidgetItem(str(self.datasfta[10][2])))
        #
        # self.tableWidget.setItem(12, 0, QTableWidgetItem(str(self.datasfta[11][0])))
        # self.tableWidget.setItem(12, 1, QTableWidgetItem(str(self.datasfta[11][1])))
        # self.tableWidget.setItem(12, 2, QTableWidgetItem(str(self.datasfta[11][2])))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Window()
    ex.show()
    sys.exit(app.exec_())