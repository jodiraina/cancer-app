from PIL import Image
import pydicom as dicom
import os, cv2

path = "test2/"
dirs = os.listdir(path)

width = 1024
height = 1024
dim = (width, height)

def resize():
    for item in dirs:
        if os.path.isfile(path+item):
            ds = dicom.dcmread(path+item)
            f, e = os.path.splitext(path+item)
            pixel_array_numpy = ds.pixel_array
            resized_img = cv2.resize(pixel_array_numpy, dim, interpolation=cv2.INTER_AREA)
            image_format = '.png'
            format = e.replace('.dcm', image_format)
            cv2.imwrite(f + format, resized_img)

resize()