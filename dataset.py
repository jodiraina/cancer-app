import cv2
import numpy as np
import matplotlib.pyplot as plt

no = str(15)
file_img = 'all-mias/mdb0'+no+'.pgm'
file_save = 'dataset/mdb0'+no+'.pgm'
img = cv2.imread(file_img)
img = cv2.flip(img, 0)

xc, yc, r = 595, 864, 68
H, W, C = img.shape
x, y = np.meshgrid(np.arange(W), np.arange(H))
d2 = (x - xc)**2 + (y - yc)**2
mask = d2 < r**2
img[mask == 0 ] = 0.0
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
cv2.imwrite(file_save,img)

print('berhasil '+no)