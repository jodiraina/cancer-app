import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import svm

cancerdata = pd.read_csv("csv2/data3.csv")
data = cancerdata.drop('Class', axis=1)
target = cancerdata['Class']

X_train, X_test, y_train, y_test = train_test_split(data, target,test_size=0.15, random_state=100)

# we create 40 separable points
X, y = make_blobs(n_samples=40, centers=2, random_state=6)

print(X)
print(y)
print(X_train)
print(y_train)
# fit the model, don't regularize for illustration purposes
clf = svm.SVC(kernel='linear')
# clf.fit(X_train, y_train)
print(clf.fit(X_train, y_train))

plt.scatter(X[:, 0], X[:, 1], c=y, s=30, cmap=plt.cm.Paired)

# # plot the decision function
# ax = plt.gca()
# xlim = ax.get_xlim()
# ylim = ax.get_ylim()
#
# # create grid to evaluate model
# xx = np.linspace(xlim[0], xlim[1], 30)
# yy = np.linspace(ylim[0], ylim[1], 30)
# YY, XX = np.meshgrid(yy, xx)
# xy = np.vstack([XX.ravel(), YY.ravel()]).T
# Z = clf.decision_function(xy).reshape(XX.shape)
#
# # plot decision boundary and margins
# ax.contour(XX, YY, Z, colors='k', levels=[-1, 0, 1], alpha=0.5,
#            linestyles=['--', '-', '--'])
# # plot support vectors
# ax.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1], s=100,
#            linewidth=1, facecolors='none', edgecolors='k')
# plt.show()